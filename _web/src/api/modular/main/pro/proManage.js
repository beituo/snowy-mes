import { axios } from '@/utils/request'

/**
 * 查询产品表
 *
 * @author 李
 * @date 2022-05-20 11:51:12
 */
export function proPage (parameter) {
  return axios({
    url: '/pro/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 产品表列表
 *
 * @author 李
 * @date 2022-05-20 11:51:12
 */
export function proList (parameter) {
  return axios({
    url: '/pro/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加产品表
 *
 * @author 李
 * @date 2022-05-20 11:51:12
 */
export function proAdd (parameter) {
  return axios({
    url: '/pro/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑产品表
 *
 * @author 李
 * @date 2022-05-20 11:51:12
 */
export function proEdit (parameter) {
  return axios({
    url: '/pro/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除产品表
 *
 * @author 李
 * @date 2022-05-20 11:51:12
 */
export function proDelete (parameter) {
  return axios({
    url: '/pro/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出产品表
 *
 * @author 李
 * @date 2022-05-20 11:51:12
 */
export function proExport (parameter) {
  return axios({
    url: '/pro/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
