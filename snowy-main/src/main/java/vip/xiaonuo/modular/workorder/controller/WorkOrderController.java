/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workorder.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.promodel.param.ProModelParam;
import vip.xiaonuo.modular.promodel.service.ProModelService;
import vip.xiaonuo.modular.workorder.param.WorkOrderParam;
import vip.xiaonuo.modular.workorder.service.WorkOrderService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 工单表控制器
 *
 * @author czw
 * @date 2022-05-25 10:31:14
 */
@RestController
public class WorkOrderController {

    @Resource
    private WorkOrderService workOrderService;

    /**
     * 查询工单表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
    @Permission
    @GetMapping("/workOrder/page")
    @BusinessLog(title = "工单表_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(WorkOrderParam workOrderParam) {
        return new SuccessResponseData(workOrderService.page(workOrderParam));
    }

    /**
     * 添加工单表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
    @Permission
    @PostMapping("/workOrder/add")
    @BusinessLog(title = "工单表_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(WorkOrderParam.add.class) WorkOrderParam workOrderParam) {
        workOrderService.add(workOrderParam);
        return new SuccessResponseData();
    }

    /**
     * 删除工单表，可批量删除
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
    @Permission
    @PostMapping("/workOrder/delete")
    @BusinessLog(title = "工单表_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(WorkOrderParam.delete.class) List<WorkOrderParam> workOrderParamList) {
        workOrderService.delete(workOrderParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑工单表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
    @Permission
    @PostMapping("/workOrder/edit")
    @BusinessLog(title = "工单表_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(WorkOrderParam.edit.class) WorkOrderParam workOrderParam) {
            workOrderService.edit(workOrderParam);
        return new SuccessResponseData();
    }

    /**
     * 查看工单表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
    @Permission
    @GetMapping("/workOrder/detail")
    @BusinessLog(title = "工单表_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(WorkOrderParam.detail.class) WorkOrderParam workOrderParam) {
        return new SuccessResponseData(workOrderService.detail(workOrderParam));
    }

    /**
     * 工单表列表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
    @Permission
    @GetMapping("/workOrder/list")
    @BusinessLog(title = "工单表_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(WorkOrderParam workOrderParam) {
        return new SuccessResponseData(workOrderService.list(workOrderParam));
    }

    /**
     * 导出系统用户
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
    @Permission
    @GetMapping("/workOrder/export")
    @BusinessLog(title = "工单表_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(WorkOrderParam workOrderParam) {
        workOrderService.export(workOrderParam);
    }

}
