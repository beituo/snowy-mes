/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.warehouse.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.xiaonuo.core.consts.CommonConstant;
import vip.xiaonuo.core.consts.SymbolConstant;
import vip.xiaonuo.core.enums.CommonStatusEnum;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.factory.TreeBuildFactory;
import vip.xiaonuo.core.pojo.node.AntdBaseTreeNode;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.invdetail.entity.InvDetail;
import vip.xiaonuo.modular.pro.entity.Pro;
import vip.xiaonuo.modular.protype.entity.ProType;
import vip.xiaonuo.modular.protype.enums.ProTypeExceptionEnum;
import vip.xiaonuo.modular.protype.param.ProTypeParam;
import vip.xiaonuo.modular.stockbalance.entity.StockBalance;
import vip.xiaonuo.modular.stockbalance.service.StockBalanceService;
import vip.xiaonuo.modular.warehouse.entity.WareHouse;
import vip.xiaonuo.modular.warehouse.enums.WareHouseExceptionEnum;
import vip.xiaonuo.modular.warehouse.mapper.WareHouseMapper;
import vip.xiaonuo.modular.warehouse.param.WareHouseParam;
import vip.xiaonuo.modular.warehouse.service.WareHouseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.util.AutoCode;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 仓库管理service接口实现类
 *
 * @author czw
 * @date 2022-07-27 16:28:21
 */
@Service
public class WareHouseServiceImpl extends ServiceImpl<WareHouseMapper, WareHouse> implements WareHouseService {

    @Resource
    StockBalanceService stockBalanceService;

    @Override
    public PageResult<WareHouse> page(WareHouseParam wareHouseParam) {
        QueryWrapper<WareHouse> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(wareHouseParam)) {

            // 根据编号 查询
            if (ObjectUtil.isNotEmpty(wareHouseParam.getCode())) {
                queryWrapper.lambda().like(WareHouse::getCode, wareHouseParam.getCode());
            }
            // 根据名称 查询
            if (ObjectUtil.isNotEmpty(wareHouseParam.getName())) {
                queryWrapper.lambda().like(WareHouse::getName, wareHouseParam.getName());
            }
            // 根据父级 查询
            if (ObjectUtil.isNotEmpty(wareHouseParam.getPid())) {
                queryWrapper.nested(item -> {
                    //查询包括自身，Pid实际为自身id
                    item.lambda().eq(WareHouse::getId, wareHouseParam.getPid()).
                            or().
                            like(WareHouse::getPids, wareHouseParam.getPid());
                });
            }
            // 根据备注 查询
            if (ObjectUtil.isNotEmpty(wareHouseParam.getRemark())) {
                queryWrapper.lambda().like(WareHouse::getRemark, wareHouseParam.getRemark());
            }
        }
        queryWrapper.orderByDesc("create_time");
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<WareHouse> list(WareHouseParam wareHouseParam) {
        return this.list();
    }

    @Override
    public void add(WareHouseParam wareHouseParam) {
        final String code = wareHouseParam.getCode();
        if (ObjectUtil.isNull(code) || ObjectUtil.isEmpty(code)) {
            wareHouseParam.setCode(AutoCode.getCodeByService("dw_ware_house", this.getClass(), 0));
        }
        //校验参数，检查是否存在相同的名称和编码
        checkParam(wareHouseParam, false);
        WareHouse wareHouse = new WareHouse();
        BeanUtil.copyProperties(wareHouseParam, wareHouse);
        this.fillPids(wareHouse);
        this.save(wareHouse);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<WareHouseParam> wareHouseParamList) {
        List<Long> wareHouseIds=new ArrayList<>();
        wareHouseParamList.forEach(wareHouseParam -> {
            //调用方法，根据id查询当前库房数据
            WareHouse wareHouse = this.queryWareHouse(wareHouseParam);
            Long id = wareHouse.getId();
            //根据节点id获取所有子节点id集合
            List<Long> childIdList = this.getChildIdListById(id);
            //如果该库房下存在子类型，则不可删除
            if (childIdList.size() >= 1) {
                throw new ServiceException(7, "【" + wareHouse.getName() + "】下存在子类型，不可删除！");
            } else {
                QueryWrapper<StockBalance> queryWrapper = new QueryWrapper<>();
                //根据仓库id查询库存余额表
                if (ObjectUtil.isNotEmpty(id)){
                    queryWrapper.lambda().eq(StockBalance::getWarHouId, id);
                }
                List<StockBalance> stockBalanceList = stockBalanceService.list(queryWrapper);
                //如果该仓库下存在库存，则不可删除
                if (stockBalanceList.size() >= 1) {
                    throw new ServiceException(8, "【" + wareHouse.getName() + "】下存在所属产品，不可删除！");
                }
            }
            wareHouseIds.add(wareHouseParam.getId());
        });
        this.removeByIds(wareHouseIds);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(WareHouseParam wareHouseParam) {
        //调用方法，根据id查询当前仓库数据
        WareHouse wareHouse = this.queryWareHouse(wareHouseParam);
        //校验参数，检查是否存在相同的名称和编码
        checkParam(wareHouseParam, true);
        BeanUtil.copyProperties(wareHouseParam, wareHouse);
        this.fillPids(wareHouse);
        //将所有子的父id进行更新
        List<Long> childIdListById = this.getChildIdListById(wareHouse.getId());
        childIdListById.forEach(subChildId -> {
            WareHouse child = this.getById(subChildId);
            WareHouseParam childParam = new WareHouseParam();
            BeanUtil.copyProperties(child, childParam);
            this.edit(childParam);
        });
        this.updateById(wareHouse);
    }

    @Override
    public WareHouse detail(WareHouseParam wareHouseParam) {
        return this.queryWareHouse(wareHouseParam);
    }

    /**
     * 获取仓库管理
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    private WareHouse queryWareHouse(WareHouseParam wareHouseParam) {
        WareHouse wareHouse = this.getById(wareHouseParam.getId());
        if (ObjectUtil.isNull(wareHouse)) {
            throw new ServiceException(WareHouseExceptionEnum.NOT_EXIST);
        }
        return wareHouse;
    }

    @Override
    public void export(WareHouseParam wareHouseParam) {
        List<WareHouse> list = this.list(wareHouseParam);
        PoiUtil.exportExcelWithStream("SnowyWareHouse.xls", WareHouse.class, list);
    }

    /**
     * 校验参数，检查是否存在相同的名称和编码
     *
     * @author xuyuxiang
     * @date 2020/3/25 21:23
     */
    private void checkParam(WareHouseParam wareHouseParam, boolean isExcludeSelf) {
        Long id = wareHouseParam.getId();
        String name = wareHouseParam.getName();
        String code = wareHouseParam.getCode();
        Long pid = wareHouseParam.getPid();

        //如果父id不是根节点
        if (!pid.equals(0L)) {
            WareHouse wareHouse = this.getById(pid);
            if (ObjectUtil.isNull(wareHouse)) {
                //父机构不存在
                throw new ServiceException(WareHouseExceptionEnum.ORG_NOT_EXIST);
            }
        }

        // isExcludeSelf为true代表是编辑，父id和自己的id不能一致
        if (isExcludeSelf) {
            if (wareHouseParam.getId().equals(wareHouseParam.getPid())) {
                throw new ServiceException(WareHouseExceptionEnum.ID_CANT_EQ_PID);
            }

            // 如果是编辑，父id不能为自己的子节点
            List<Long> childIdListById = this.getChildIdListById(wareHouseParam.getId());
            if (ObjectUtil.isNotEmpty(childIdListById)) {
                if (childIdListById.contains(wareHouseParam.getPid())) {
                    throw new ServiceException(WareHouseExceptionEnum.PID_CANT_EQ_CHILD_ID);
                }
            }
        }

        LambdaQueryWrapper<WareHouse> queryWrapperByName = new LambdaQueryWrapper<>();
        queryWrapperByName.eq(WareHouse::getName, name);

        LambdaQueryWrapper<WareHouse> queryWrapperByCode = new LambdaQueryWrapper<>();
        queryWrapperByCode.eq(WareHouse::getCode, code);
        //如果是编辑，校验名称编码时排除自身
        if (isExcludeSelf) {
            queryWrapperByName.ne(WareHouse::getId, id);
            queryWrapperByCode.ne(WareHouse::getId, id);
        }

        int countByName = this.count(queryWrapperByName);
        int countByCode = this.count(queryWrapperByCode);

        if (countByName >= 1) {
            throw new ServiceException(WareHouseExceptionEnum.ORG_NAME_REPEAT);
        }
        if (countByCode >= 1) {
            throw new ServiceException(WareHouseExceptionEnum.ORG_CODE_REPEAT);
        }
    }


    @Override
    public List<AntdBaseTreeNode> tree(WareHouseParam wareHouseParam) {
        List<AntdBaseTreeNode> treeNodeList = CollectionUtil.newArrayList();
        //按AntdBaseTreeNode要求构建树形的数据结构
        this.list().forEach(wareHouse -> {
            AntdBaseTreeNode proTreeNode = new AntdBaseTreeNode();
            proTreeNode.setId(wareHouse.getId());
            proTreeNode.setParentId(wareHouse.getPid());
            proTreeNode.setTitle(wareHouse.getName());
            proTreeNode.setValue(String.valueOf(wareHouse.getId()));
            treeNodeList.add(proTreeNode);
        });


        return new TreeBuildFactory<AntdBaseTreeNode>().doTreeBuild(treeNodeList);
    }

    /**
     * 根据节点id获取所有子节点id集合
     *
     * @author xuyuxiang
     * @date 2020/3/26 11:31
     */
    @Override
    public List<Long> getChildIdListById(Long id) {
        List<Long> childIdList = CollectionUtil.newArrayList();
        LambdaQueryWrapper<WareHouse> queryWrapper = new LambdaQueryWrapper<>();
        //查询为节点id子集的数据
        queryWrapper.like(WareHouse::getPids, SymbolConstant.LEFT_SQUARE_BRACKETS + id +
                SymbolConstant.RIGHT_SQUARE_BRACKETS);
        this.list(queryWrapper).forEach(wareHouse -> childIdList.add(wareHouse.getId()));
        return childIdList;
    }


    /**
     * 填充父ids
     *
     * @author xuyuxiang
     * @date 2020/3/26 11:28
     */
    private void fillPids(WareHouse wareHouse) {
        //如果是一级菜单，则添加[0]
        if (wareHouse.getPid().equals(0L)) {
            wareHouse.setPids(SymbolConstant.LEFT_SQUARE_BRACKETS +
                    0 +
                    SymbolConstant.RIGHT_SQUARE_BRACKETS +
                    SymbolConstant.COMMA);
        } else {
            //获取父类型数据
            WareHouse wareHouse1 = this.getById(wareHouse.getPid());
            //将父类型的所有父类中拼接上自身的id，即为当前添加类型的所有父类
            wareHouse.setPids(wareHouse1.getPids() +
                    SymbolConstant.LEFT_SQUARE_BRACKETS + wareHouse1.getId() +
                    SymbolConstant.RIGHT_SQUARE_BRACKETS +
                    SymbolConstant.COMMA);
        }
    }

}
