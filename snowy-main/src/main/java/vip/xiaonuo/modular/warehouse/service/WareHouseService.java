/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.warehouse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.core.pojo.node.AntdBaseTreeNode;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.modular.protype.param.ProTypeParam;
import vip.xiaonuo.modular.warehouse.entity.WareHouse;
import vip.xiaonuo.modular.warehouse.param.WareHouseParam;
import java.util.List;

/**
 * 仓库管理service接口
 *
 * @author czw
 * @date 2022-07-27 16:28:21
 */
public interface WareHouseService extends IService<WareHouse> {

    /**
     * 查询仓库管理
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    PageResult<WareHouse> page(WareHouseParam wareHouseParam);

    /**
     * 仓库管理列表
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    List<WareHouse> list(WareHouseParam wareHouseParam);

    /**
     * 添加仓库管理
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    void add(WareHouseParam wareHouseParam);

    /**
     * 删除仓库管理
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    void delete(List<WareHouseParam> wareHouseParamList);

    /**
     * 编辑仓库管理
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    void edit(WareHouseParam wareHouseParam);

    /**
     * 查看仓库管理
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
     WareHouse detail(WareHouseParam wareHouseParam);

    /**
     * 导出仓库管理
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
     void export(WareHouseParam wareHouseParam);

    /**
     * 获取系统组织机构树
     *
     * @param  wareHouseParam 查询参数
     * @return 仓库管理树
     * @author czw
     * @date 2022/7/28 16:27
     */
    List<AntdBaseTreeNode> tree(WareHouseParam wareHouseParam);

    /**
     * 根据节点id获取所有子节点id集合
     *
     * @author xuyuxiang
     * @date 2020/3/26 11:31
     */
    List<Long> getChildIdListById(Long id);

}
