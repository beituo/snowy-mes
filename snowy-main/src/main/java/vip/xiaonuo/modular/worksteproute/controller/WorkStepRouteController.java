/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.worksteproute.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.workroute.entity.WorkRoute;
import vip.xiaonuo.modular.workroute.param.WorkRouteParam;
import vip.xiaonuo.modular.workroute.service.WorkRouteService;
import vip.xiaonuo.modular.workstep.param.WorkStepParam;
import vip.xiaonuo.modular.workstep.service.WorkStepService;
import vip.xiaonuo.modular.worksteproute.param.WorkStepRouteParam;
import vip.xiaonuo.modular.worksteproute.service.WorkStepRouteService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 工序路线关系表控制器
 *
 * @author 楊楊
 * @date 2022-05-27 15:12:44
 */
@RestController
public class WorkStepRouteController {

    @Resource
    private WorkStepRouteService workStepRouteService;
    @Resource
    private WorkStepService workStepService;
    @Resource
    WorkRouteService workRouteService;
    /**
     * 查询工序路线关系表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
    @Permission
    @GetMapping("/workStepRoute/page")
    @BusinessLog(title = "工序路线关系表_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(WorkStepRouteParam workStepRouteParam) {
        return new SuccessResponseData(workStepRouteService.page(workStepRouteParam));
    }

    /**
     * 添加工序路线关系表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
    @Permission
    @PostMapping("/workStepRoute/add")
    @BusinessLog(title = "工序路线关系表_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(WorkStepRouteParam.add.class) WorkStepRouteParam workStepRouteParam) {
            workStepRouteService.add(workStepRouteParam);
        return new SuccessResponseData();
    }

    /**
     * 删除工序路线关系表，可批量删除
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
    @Permission
    @PostMapping("/workStepRoute/delete")
    @BusinessLog(title = "工序路线关系表_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(WorkStepRouteParam.delete.class) List<WorkStepRouteParam> workStepRouteParamList) {
            workStepRouteService.delete(workStepRouteParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑工序路线关系表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
    @Permission
    @PostMapping("/workStepRoute/edit")
    @BusinessLog(title = "工序路线关系表_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(WorkStepRouteParam.edit.class) WorkStepRouteParam workStepRouteParam) {
            workStepRouteService.edit(workStepRouteParam);
        return new SuccessResponseData();
    }

    /**
     * 查看工序路线关系表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
    @Permission
    @GetMapping("/workStepRoute/detail")
    @BusinessLog(title = "工序路线关系表_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(WorkStepRouteParam.detail.class) WorkStepRouteParam workStepRouteParam) {
        return new SuccessResponseData(workStepRouteService.detail(workStepRouteParam));
    }

    /**
     * 工序路线关系表列表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
    @Permission
    @GetMapping("/workStepRoute/list")
    @BusinessLog(title = "工序路线关系表_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(WorkStepRouteParam workStepRouteParam) {
        return new SuccessResponseData(workStepRouteService.list(workStepRouteParam));
    }

    /**
     * 导出系统用户
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
    @Permission
    @GetMapping("/workStepRoute/export")
    @BusinessLog(title = "工序路线关系表_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(WorkStepRouteParam workStepRouteParam) {
        workStepRouteService.export(workStepRouteParam);
    }

}
