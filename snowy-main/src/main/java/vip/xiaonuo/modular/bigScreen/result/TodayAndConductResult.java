package vip.xiaonuo.modular.bigScreen.result;

import lombok.Data;

@Data
public class TodayAndConductResult {
    //在制工单数量
    private  Integer conductWorkOrderCount;
    //延期工单数
    private  Integer delayWorkOrderCount;
    //在制任务数
    private  Integer conductTaskCount;
    //延期任务数
    private  Integer delayTaskCount;
    //今日不良品率
    private  Double rejectsRate;
    //今日完成工单
    private  Integer workOrderFinishNum;
    //今日工单产出
    private  Integer workOrderProNum;
    //今日完成任务
    private  Integer taskFinishCount;
    //在制任务计划数
    private  Integer taskPalNum;
    //在制工单计划数
    private  Integer workOrderPalNum;




}
