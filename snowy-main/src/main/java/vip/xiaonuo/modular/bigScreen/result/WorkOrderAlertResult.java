package vip.xiaonuo.modular.bigScreen.result;

import lombok.Data;

@Data
public class WorkOrderAlertResult {
    //剩余时间
    private Integer surplusTime;
    //产品名称
    private String name;
    //工单编号
    private String workOrderNo;

}
